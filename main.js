const os = require('os')
const Chance = require('chance')
const jwt = require('jsonwebtoken')
const env = require('node-env-file')
const chance = new Chance()

env(__dirname + '/.env') //link env file

let user = {
  '_id': '47a81195-c1a3-585f-a238-84a67d99f4f1',
  'name': 'Reekoh QA',
  'email': 'qa@reekoh.com',
  'account': '70db64c8-90ba-52ec-a4c4-bbc64ffd3f4e',
  'accountName': 'Reekoh',
  'role': 'ce9cfde8-3c67-543c-9fcc-028f097baa4e',
  'roleName': 'Admin',
  'lastIp': '10.240.0.15',
  'lastLogin': '2018-02-18T05:10:21.269Z',
  'createdDate': '2017-11-09T06:24:49.480Z',
  'updatedDate': '2017-11-09T06:25:12.383Z'
}

let resources = [
  'accounts',
  'channels',
  'command-logs',
  'command-relays',
  'command-templates',
  'connectors',
  'converters',
  'dashboards',
  'datasets',
  'device-certs',
  'device-commands',
  'device-groups',
  'devices',
  'exception-loggers',
  'exceptions',
  'files',
  'filters',
  'gateways',
  'inventory-sync',
  'loggers',
  'logs',
  'pipeline-templates',
  'pipelines',
  'plugins',
  'roles',
  'resources',
  'releases',
  'server-certs',
  'services',
  'system-logs',
  'storages',
  'streams',
  'tokens',
  'triggers',
  'user-groups',
  'users'
]

user.permissions = {}

resources.map(resource => {
  user.permissions[resource] = 'admin'
})

let token = jwt.sign(user, Buffer.from(process.env.JWT_SECRET, 'base64'), {
  algorithm: 'HS512',
  expiresIn: '999y',
  audience: process.env.BASE_URL,
  issuer: 'https://reekoh.com',
  jwtid: chance.guid(),
  subject: user._id
})

console.log(`Bearer ${token}`)
